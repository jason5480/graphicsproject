#include <stdio.h>      // Just for some ASCII messages
#include <cstring>      // For string handling
#include "window.h"     // Header file for OpenGL window data
#include "visuals.h"    // Header file for our OpenGL functions
#include "Model_OBJ.h"  // Header file for Model_OBJ functions
#include <glut.h>       // An interface and windows management library

enum KEYBOARD
{
  KEY_ESC = 27, KEY_ms =  45,
  KEY_eq  = 61, KEY_bs =  92,
  KEY_1   = 49, KEY_2, KEY_3,
  KEY_a   = 97, KEY_b, KEY_c,
  KEY_d, KEY_e, KEY_f, Key_g, KEY_h,
  KEY_i, KEY_j, KEY_k, KEY_l, KEY_m,
  KEY_n, KEY_o, KEY_p, KEY_q, KEY_r,
  KEY_s, KEY_t, KEY_u, KEY_v, KEY_w,
  KEY_x, KEY_y, KEY_z
};

/*******************************************************************************
  State Variables
 ******************************************************************************/

extern GlutWindow win;                        // Declaration of the variable
Model_OBJ unicorn("MyModels/Model_1.obj");    // Definition of the variable and Loads model
Model_OBJ helicopter("MyModels/Model_2.obj"); // Definition of the variable and Loads model

static int   hierarchy        = 0;
static bool  show_uni_state   = true;
static bool  show_hel_state   = true;
static bool  move_uni_state   = true;
static bool  move_hel_state   = true;
static bool  show_axes_state  = true;
static bool  show_sphere_state=false;
static bool  show_box_state   = false;
static bool  yellow_state     = true;
static bool  red_state        = true;
static bool  green_state      = true;
static bool  blue_state       = true;
static bool  mouse_state      = false;
static int   mx0             = 0;
static int   my0             = 0;
// Camera variables
static float rotFactor = 0.001;
static float zoomFactor  = 0.1;
static float cdist = 250.0;
static float cx = 0.0;
static float cz = cdist;
static float angle = 0.0;

/*******************************************************************************
  Implementation of Visual Functions
 ******************************************************************************/

void Axes()
{
  glColor3f(0.6, 0.6, 0.6);
  glBegin(GL_LINES);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(100.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 100.0, 0.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 100.0);
  glEnd();
  glPushMatrix();
    glTranslatef(90, -2.0 , 0.0);
    glScalef(0.06, 0.06, 0.06);
    glutStrokeCharacter(GLUT_STROKE_ROMAN, 'x');
  glPopMatrix();
  glPushMatrix();
    glTranslatef(-2.0, 90 , 0.0);
    glScalef(0.06, 0.06, 0.06);
    glutStrokeCharacter(GLUT_STROKE_ROMAN, 'y');
  glPopMatrix();
  glPushMatrix();
    glTranslatef(-2.0, -2.0 , 90);
    glScalef(0.06, 0.06, 0.06);
    glutStrokeCharacter(GLUT_STROKE_ROMAN, 'z');
  glPopMatrix();
}

//------------------------------------------------------------------------------

void Field()
{
  glColor3f(0.3, 0.7, 0.3);
  glPushMatrix();
    glTranslatef(0, -1, 0);
    glBegin(GL_TRIANGLE_FAN);
    glVertex3f(0.0f,0.0f,0.0f);
    glVertex3f(100.0f,0.0f,100.0f);
    glVertex3f(100.0f,0.0f,-100.0f);
    glVertex3f(-100.0f,0.0f,-100.0f);
    glVertex3f(-100.0f,0.0f,100.0f);
    glVertex3f(100.0f,0.0f,100.0f);
    glEnd();
  glPopMatrix();
}

//------------------------------------------------------------------------------

void Setup()
{
  /* Set up the objects */
  unicorn.Load();
  unicorn.SetColor(0.7, 0.7, 0.7);
  unicorn.CalibrateTran();
  unicorn.CalibrateRot(0.0, 90.0, 0.0);
  unicorn.Translate(0.0, 40.0, 0.0);
  unicorn.CalculateMax();
  unicorn.CalculateRadius();
  helicopter.Load();
  helicopter.SetColor(0.3, 0.3, 0.8);
  helicopter.Scale(100, 100, 100);
  helicopter.CalibrateTran();
  helicopter.CalibrateRot(90.0, 180.0, 0.0);
  helicopter.Translate(0.0, 100.0, 0.0);
  helicopter.CalculateMax();
  helicopter.CalculateRadius();

  glShadeModel(GL_SMOOTH);  // Parameter handling

  glEnable(GL_DEPTH_TEST);
  // renders a fragment if its z value is less or equal of the stored value
  glDepthFunc(GL_LEQUAL);
  glClearDepth(1.0f);

  /* polygon rendering mode */
  glEnable(GL_COLOR_MATERIAL);
  glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

  /* Set up light source */
  glLoadIdentity();

  /* Spot Light0 Yellow */
  GLfloat light0_ambient[] = { 0.2, 0.2, 0.0, 1.0 };
  GLfloat light0_diffuse[] = { 0.6, 0.6, 0.1, 1.0 };
  GLfloat light0_specular[] = { 1.0, 1.0, 0.2, 1.0 };
  GLfloat light0_position[] = { 100, 50.0, 0.0, 1.0 };
  GLfloat light0_direction[]= { -0.35, -0.229, -0.875, 0.0 };
  glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, light0_specular);
  glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
  glLightf (GL_LIGHT0, GL_SPOT_CUTOFF, 20.0);
  glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light0_direction);

  /* Spot Light1 Red*/
  GLfloat light1_ambient[] = { 0.2, 0.0, 0.0, 1.0 };
  GLfloat light1_diffuse[] = { 0.6, 0.0, 0.0, 1.0 };
  GLfloat light1_specular[] = { 1.0, 0.0, 0.0, 1.0 };
  GLfloat light1_position[] = { -100, 50.0, 0.0, 1.0 };
  GLfloat light1_direction[]= { 0.35, -0.229, -0.875, 0.0 };
  glLightfv(GL_LIGHT1, GL_AMBIENT, light1_ambient);
  glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
  glLightfv(GL_LIGHT1, GL_SPECULAR, light1_specular);
  glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
  glLightf (GL_LIGHT1, GL_SPOT_CUTOFF, 20.0);
  glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1_direction);

  /* Spot Light2 Green */
  GLfloat light2_ambient[] = { 0.0, 0.2, 0.0, 1.0 };
  GLfloat light2_diffuse[] = { 0.0, 0.6, 0.0, 1.0 };
  GLfloat light2_specular[] = { 0.0, 1.0, 0.0, 1.0 };
  GLfloat light2_position[] = { -100, -50.0, 0.0, 1.0 };
  GLfloat light2_direction[]= { 0.35, 0.229, -0.875, 0.0 };
  glLightfv(GL_LIGHT2, GL_AMBIENT, light2_ambient);
  glLightfv(GL_LIGHT2, GL_DIFFUSE, light2_diffuse);
  glLightfv(GL_LIGHT2, GL_SPECULAR, light2_specular);
  glLightfv(GL_LIGHT2, GL_POSITION, light2_position);
  glLightf (GL_LIGHT2, GL_SPOT_CUTOFF, 20.0);
  glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2_direction);

  /* Spot Light3 Blue*/
  GLfloat light3_ambient[] = { 0.0, 0.0, 0.2, 1.0 };
  GLfloat light3_diffuse[] = { 0.0, 0.0, 0.6, 1.0 };
  GLfloat light3_specular[] = { 0.0, 0.0, 1.0, 1.0 };
  GLfloat light3_position[] = { 100, -50.0, 0.0, 1.0 };
  GLfloat light3_direction[]= { -0.35, 0.229, -0.875, 0.0 };
  glLightfv(GL_LIGHT3, GL_AMBIENT, light3_ambient);
  glLightfv(GL_LIGHT3, GL_DIFFUSE, light3_diffuse);
  glLightfv(GL_LIGHT3, GL_SPECULAR, light3_specular);
  glLightfv(GL_LIGHT3, GL_POSITION, light3_position);
  glLightf (GL_LIGHT3, GL_SPOT_CUTOFF, 20.0);
  glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, light3_direction);

  glEnable(GL_LIGHTING);

  GLfloat specref[] = { 0.8, 0.8, 0.8, 1.0 };
  GLint shininess = 64.0;
  glMaterialfv(GL_FRONT,GL_SPECULAR,specref);
  glMaterialf(GL_FRONT,GL_SHININESS,shininess);

  // Blending
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  /* Black background */
  glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
}

//------------------------------------------------------------------------------

void Render()
{
  // Clean up the colour of the window and the depth buffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // Set the camera
  gluLookAt(cx, 50, cz,  // camera position
            0,  50,  0,  // target position
            0,   1,  0); // up vector

  Axes();
  Field();

  /* Teapot */
  glPushMatrix();
    glTranslatef(0.0, 6.0, 0.0);  // Center The spesific Object axes
    glColor3f(0.5, 0.3, 0.3);
    glutSolidTeapot(10.0);
  glPopMatrix();

  /* Model_1 unicorn */
  if (show_uni_state)
  {
    unicorn.DrawModel();
    if (show_axes_state)   unicorn.DrawAxes();
    if (show_sphere_state) unicorn.DrawSphere(hierarchy);
    if (show_box_state)    unicorn.DrawBox(hierarchy);

  }

  /* Model_2 helicopter */
  if (show_hel_state)
  {
    helicopter.DrawModel();
    if (show_axes_state)   helicopter.DrawAxes();
    if (show_sphere_state) helicopter.DrawSphere(hierarchy);
    if (show_box_state)    helicopter.DrawBox(hierarchy);
  }

  /* Enable and Disable Lights */
  if (yellow_state) glEnable(GL_LIGHT0);
  else glDisable(GL_LIGHT0);
  if (red_state) glEnable(GL_LIGHT1);
  else glDisable(GL_LIGHT1);
  if (green_state) glEnable(GL_LIGHT2);
  else glDisable(GL_LIGHT2);
  if (blue_state) glEnable(GL_LIGHT3);
  else glDisable(GL_LIGHT3);

  // All drawing commands applied to the
  // hidden buffer, so now, bring forward
  // the hidden buffer and hide the visible one
  glutSwapBuffers();
}

//------------------------------------------------------------------------------

void Idle()
{
  glutPostRedisplay();
}

//------------------------------------------------------------------------------

void MenuSelect(int choice)
{
  switch (choice)  // Assingn menu options functionality
  {
    case BOTH :
      show_uni_state = true;
      show_hel_state = true;
      break;
    case UNICORN :
      show_uni_state = true;
      show_hel_state = false;
      break;
    case HELICOPTER :
      show_uni_state = false;
      show_hel_state = true;
      break;
    case SOLID :
      if (move_uni_state) unicorn.net_mode    = false;
      if (move_hel_state) helicopter.net_mode = false;
      break;
    case WIRE :
      if (move_uni_state) unicorn.net_mode    = true;
      if (move_hel_state) helicopter.net_mode = true;
      break;
    case YLIGHT :
      yellow_state = true;
      red_state    = false;
      green_state  = false;
      blue_state   = false;
      break;
    case RLIGHT :
      yellow_state = false;
      red_state    = true;
      green_state  = false;
      blue_state   = false;
      break;
    case GLIGHT :
      yellow_state = false;
      red_state    = false;
      green_state  = true;
      blue_state   = false;
      break;
    case BLIGHT :
      yellow_state = false;
      red_state    = false;
      green_state  = false;
      blue_state   = true;
      break;
    case ALIGHT :
      yellow_state = true;
      red_state    = true;
      green_state  = true;
      blue_state   = true;
      break;
  }
}

//------------------------------------------------------------------------------

void KeyboardDown(unsigned char key, int x, int y)
{
  switch (key)  // Assingn key functionality when pressed
  {
    case KEY_ESC:
      exit(0);
      break;
    case KEY_u:
      move_uni_state = false;
      break;
    case KEY_h:
      move_hel_state = false;
      break;
    case KEY_q:
      if (move_uni_state) helicopter.Translate(0.0, 0.0, -0.5);
      if (move_hel_state) unicorn.Translate(0.0, 0.0, -0.5);
      break;
    case KEY_w:
      if (move_uni_state) helicopter.Translate(0.0, 0.5, 0.0);
      if (move_hel_state) unicorn.Translate(0.0, 0.5, 0.0);
      break;
    case KEY_e:
      if (move_uni_state) helicopter.Translate(0.0, 0.0, 0.5);
      if (move_hel_state) unicorn.Translate(0.0, 0.0, 0.5);
      break;
    case KEY_a:
      if (move_uni_state) helicopter.Translate(-0.5, 0.0, 0.0);
      if (move_hel_state) unicorn.Translate(-0.5, 0.0, 0.0);
      break;
    case KEY_s:
      if (move_uni_state) helicopter.Translate(0.0, -0.5, 0.0);
      if (move_hel_state) unicorn.Translate(0.0, -0.5, 0.0);
      break;
    case KEY_d:
      if (move_uni_state) helicopter.Translate(0.5, 0.0, 0.0);
      if (move_hel_state) unicorn.Translate(0.5, 0.0, 0.0);
      break;
    case KEY_bs:
      show_axes_state = !show_axes_state;
      break;
    case KEY_x:
      show_sphere_state = !show_sphere_state;
      break;
    case KEY_z:
      show_box_state = !show_box_state;
      break;
    case KEY_v:
      if (!move_uni_state) unicorn.PrintVertices();
      if (!move_hel_state) helicopter.PrintVertices();
      break;
    case KEY_t:
      if (!move_uni_state) unicorn.PrintTriangles();
      if (!move_hel_state) helicopter.PrintTriangles();
      break;
    case KEY_1:
      if (!move_uni_state) unicorn.LofD(1);
      if (!move_hel_state) helicopter.LofD(1);
      break;
    case KEY_2:
      if (!move_uni_state) unicorn.LofD(2);
      if (!move_hel_state) helicopter.LofD(2);
      break;
    case KEY_3:
      if (!move_uni_state) unicorn.LofD(10);
      if (!move_hel_state) helicopter.LofD(10);
      break;
    case KEY_c:
      helicopter.DetectCollisions(unicorn);
      break;
    case KEY_l:
      if (!move_uni_state)
        for (int i = 0; i < pow(2, hierarchy); i++)
          unicorn.BoundingSphere[i]->coverage();
      if (!move_hel_state)
        for (int i = 0; i < pow(2, hierarchy); i++)
          helicopter.BoundingSphere[i]->coverage();
      break;
    case KEY_k:
      if (!move_uni_state)
        for (int i = 0; i < pow(2, hierarchy); i++)
          unicorn.AABB[i]->coverage();
      if (!move_hel_state)
        for (int i = 0; i < pow(2, hierarchy); i++)
          helicopter.AABB[i]->coverage();
      break;
    case KEY_eq:
      if (hierarchy < 3) hierarchy++;
      break;
    case KEY_ms:
      if (hierarchy > 0) hierarchy--;
      break;
    default:
      break;
  }
  glutPostRedisplay();
}

//------------------------------------------------------------------------------

void KeyboardUp(unsigned char key, int x, int y)
{
  switch (key)  // Assingn key functionality when released
  {
    case KEY_u:
      move_uni_state = true;
      break;
    case KEY_h:
      move_hel_state = true;
      break;
    default:
      break;
  }
}

//------------------------------------------------------------------------------

void KeyboardSpecialDown(int key, int x, int y)
{
  switch(key)
  {
    case GLUT_KEY_UP:
      if (move_uni_state)
      {
        helicopter.RotateX(-0.5);
      }
      if (move_hel_state)
      {
        unicorn.RotateX(-0.5);
      }
      break;
    case GLUT_KEY_DOWN:
      if (move_uni_state)
      {
        helicopter.RotateX(0.5);
      }
      if (move_hel_state)
      {
        unicorn.RotateX(0.5);
      }
      break;
    case GLUT_KEY_RIGHT:
      if (move_uni_state)
      {
        helicopter.RotateY(0.5);
      }
      if (move_hel_state)
      {
        unicorn.RotateY(0.5);
      }
      break;
    case GLUT_KEY_LEFT:
      if (move_uni_state)
      {
        helicopter.RotateY(-0.5);
      }
      if (move_hel_state)
      {
        unicorn.RotateY(-0.5);
      }
      break;
    default:
      break;
  }
  glutPostRedisplay();
  //*/
}

//------------------------------------------------------------------------------

void KeyboardSpecialUp(int key, int x, int y)
{
}

//------------------------------------------------------------------------------

void MouseClick(int button, int state, int x, int y)
{
  if (state == GLUT_DOWN && button == GLUT_LEFT_BUTTON)// Initiate new rotation
  {
    mouse_state = true;
    mx0 = x;
    my0 = y;
  }
  else if (state == GLUT_UP && button == GLUT_LEFT_BUTTON)      // Stop rotate
  {
    mouse_state = false;
  }
}

//------------------------------------------------------------------------------

void MouseMotion(int x, int y)
{
  if (mouse_state)  // Rotation depends on current cursor position
  {
    cdist += (y-my0)*zoomFactor;
    angle -= (x-mx0)*rotFactor;
    cx = sin(angle)*cdist;
    cz = cos(angle)*cdist;
    mx0 = x;
    my0 = y;

    glutPostRedisplay();
    /*
    if (move_uni_state)
    {
      helicopter.RotateY(0.5*(x - M_X0));
      helicopter.RotateX(0.5*(y - M_Y0));
    }
    if (move_hel_state)
    {
      unicorn.RotateY(0.5*(x - M_X0));
      unicorn.RotateX(0.5*(y - M_Y0));
    }
    M_X0 = x;
    M_Y0 = y;
    glutPostRedisplay();
    //*/
  }
}

//------------------------------------------------------------------------------

void Resize(int w, int h)
{
  /* define the visible area of the window ( in pixels ) */
  if (h==0) h=1;
  win.width = w;
  win.height= h;
  glViewport(0, 0, win.width, win.height);

  /* Setup viewing volume */
  GLfloat aspect = (GLfloat) win.width / win.height;
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  gluPerspective(win.field_of_view_angle, aspect, win.z_near, win.z_far);
}
