#include <vector>
/*******************************************************************************
  Class Declarations
 ******************************************************************************/

class Model_OBJ                                                       // Class Model_OBJ
{
public:
  Model_OBJ(char *filename);                                          // Constructor
  ~Model_OBJ();                                                       // Detructor Release the model

  char *filename;                                                     // Name of the file
  bool  net_mode;                                                     // Sets the Drawing mode Net or Solid
  class Sphere **BoundingSphere;                                      // Stores pointers to bounding spheres of each hierarchy
  class Box    **AABB;                                                // Stores pointer to axis aligned bounding boxes of each hierarchy

  void  Load(void);                                                   // Loads the model
  void  DrawModel(void);                                              // Draws the model on the screen
  void  DrawAxes(void);                                               // Draws the models axes
  void  DrawSphere(int num);                                          // Draws the models bounding sphere
  void  DrawBox(int num);                                             // Draws the models axis aligned bounding box
  void  SetColor(float red, float green, float blue);                 // Sets the objects color in RGB
  void  CalibrateTran(void);                                          // Center the model
  void  CalibrateRot(float thx, float thy, float thz);                // Fix the XYZ axes direction
  void  Translate(float dx, float dy, float dz);                      // Translate the model
  void  RotateX(float theta);                                         // Rotate the model around X
  void  RotateY(float theta);                                         // Rotate the model around Y
  void  RotateZ(float theta);                                         // Rotate the model around Z
  void  Scale(float Xscale, float Yscale, float Zscale);              // Scale the model
  void  LofD(int factor);                                             // Sets the Level of Detail
  void  DetectCollisions(const Model_OBJ &other);                     // Detects the number of collisions bewteen this and other model
  void  SphereCoverage();                                             // Calculate the percentage volume coverage of bounding sphere
  void  BoxCoverage();                                                // Calculate the percentage volume coverage of axis aligned bounding box
  void  CalculateMax(void);                                           // Calculates the maximum X Y Z dimensions from the center
  void  CalculateRadius(void);                                        // Calculates the maximum radius from the center
  void  PrintTriangles(void);                                         // Prints the triangleBuffer
  void  PrintVertices(void);                                          // Prints the vertexBuffer

private:
  bool  anomaly, system;                                              // Object has anomaly or not, CW or CCW system
  class Triangle **triangleBuffer;                                    // Stores the faces which make the object
  class Vector3f **vertexBuffer;                                      // Stores the points which make the object
  long  TotalVertices;                                                // Stores the total number of vertices
  long  TotalTriangles;                                               // Stores the total number of triangles
  float red, green, blue;                                             // Stores the RGB color
  class Vector3f *Center;                                             // Stores the current center of the model
  class Vector3f *Max;                                                // Stores the maximum distance from the center
  float Radius;                                                       // Stores the maximum radiuse from the center
  float rotx, roty, rotz;                                             // Stores the current rotation of the model

  void  SetAngles(void);                                              // Creates the angle array
  void  ResetModel(void);                                             // Reset all the flags
  Vector3f* DefineDeleteVertex(void);                                 // Define vertex gonna delete
  Vector3f* DefineReplaceVertex(Vector3f *va);                        // Define vertex gonna replace va
  long  Replace(Vector3f *va, Vector3f *vb);                          // Replace vertex va with vertex vb in all triangles
  float CalculateAngle(Vector3f *va, Vector3f *vb);                   // Calculates the angle between two vectors

  friend class Triangle;
  friend class Sphere;
  friend class Box;
};

class Triangle                                                        // Class Triangle
{
private:
  Triangle();                                                         // Constructor
  ~Triangle();                                                        // Detructor

  bool exist, change, intersected;                                    // Triangle is active or not
  int  vnum1, vnum2, vnum3;                                           // Number of each vertex in vertexBuffer
  class Vector3f *vertex1;                                            // Points at first  vertex
  class Vector3f *vertex2;                                            // Points at second vertex
  class Vector3f *vertex3;                                            // Points at third  vertex
  class Vector3f *normal;                                             // Normal for this Triangle and replace vertex

  void setMeAsNeighbour(Model_OBJ &model);                            // Set neighbours for each vertex
  void setVertices(Model_OBJ &model);                                 // Calculates vertices from vertexBuffer
  void setNormal(bool system);                                        // Calculates normal for this triangle
  bool intersect(Triangle &other, bool system);                       // Triangle-Triangle Intersection
  bool intersect(Vector3f &lp1, Vector3f &lp2, bool system);          // Ray-Triangle Intersection

  friend class Model_OBJ;
  friend class Sphere;
  friend class Box;
};

class Vector3f                                                        // Class Vector3f
{
private:
  Vector3f();                                                         // Constructor1
  Vector3f(float x, float y, float z);                                // Constructor2
  ~Vector3f();                                                        // Detructor

  bool  exist;                                                        // Vertex is active or not
  float x, y, z;                                                      // Coordinates X Y Z
  float angle;                                                        // Stores the angle between neighbours for this vertex
  std::vector<Triangle*> Neighbour;                                   // Stores the triagles where belongs

  void  set(float x, float y, float z);                               // Sets x, y, z
  float length(void);                                                 // Distance from (0, 0, 0)
  void  normalize(void);                                              // Fix x, y, z so that value |v| = 1

  Vector3f operator + (const Vector3f &other);                        // Vector operations
  Vector3f operator - (const Vector3f &other);
  Vector3f operator * (const Vector3f &other);
  Vector3f operator / (const Vector3f &other);

  Vector3f operator * (const float scalar);
  friend Vector3f operator * (const float scalar, const Vector3f &other);

  Vector3f& operator  = (const Vector3f &other);
  Vector3f& operator += (const Vector3f &other);
  Vector3f& operator -= (const Vector3f &other);

  Vector3f operator + (void) const;
  Vector3f operator - (void) const;

  float distance(const Vector3f &other);                              // Distance between this and other vertex
  float dot(const Vector3f &other);                                   // Dot product this.other
  Vector3f cross(const Vector3f &other);                              // Cross product this*other

  friend class Model_OBJ;
  friend class Triangle;
  friend class Sphere;
  friend class Box;
};

class Sphere
{
public:
  Sphere();
  ~Sphere();

  Vector3f  *center;
  float radius;
  Model_OBJ *model;
  void  set(Model_OBJ *model, Vector3f *center, float radius);
  void  draw();                                                       // Draws the models bounding sphere
  void  coverage();                                                   // Calculate the percentage volume coverage of bounding sphere
};

class Box
{
public:
  Box();
  ~Box();

  Vector3f  *center;
  Vector3f  *max;
  Model_OBJ *model;
  void  set(Model_OBJ *model, Vector3f *center, Vector3f *max);
  void  draw();                                                       // Draws the models bounding sphere
  void  coverage();                                                   // Calculate the percentage volume coverage of bounding sphere
};