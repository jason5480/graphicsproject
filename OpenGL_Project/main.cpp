//----------------------------------------------------------------------------//
//                                                                            //
//          On/mo : Nikolas Iason                                             //
//          A.M.  : 7078                                                      //
//                                                                            //
//          Graphics OpenGL: Apalaktiko Project                               //
//                                                                            //
//          Level Of Detail Project                                           //
//----------------------------------------------------------------------------//

#include <stdlib.h>   // Just for some standard functions
#include <stdio.h>    // Just for some ASCII messages
#include <glut.h>     // An interface and windows management library
#include "window.h"   // Header file for OpenGL window data
#include "visuals.h"  // Header file for our OpenGL functions

/*******************************************************************************
Main Program code
*******************************************************************************/

// Definition of the variable
GlutWindow win;

int main(int argc, char **argv)
{
  /* set window values */
  win.width               = 640;
  win.height              = 480;
  win.title               = "OpenGL Project.";
  win.field_of_view_angle = 45;
  win.z_near              = 1.0f;
  win.z_far               = 500.0f;

  /* initialize and run program */
  // GLUT initialization
  glutInit(&argc, argv);

  // Display Mode //RGBA//DEPTH BUFFER//DOUBLE BUFFER//
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );

  // set window size
  glutInitWindowSize(win.width,win.height);
  // window initial position
  glutInitWindowPosition(650,50);

  // create Window
  glutCreateWindow(win.title);

  // Configure various properties of the OpenGL rendering context
  Setup();

  // register Display Function
  glutDisplayFunc(Render);
  // register Reshape Function
  glutReshapeFunc(Resize);
  // register Idle Function
  glutIdleFunc(Idle);
  // register Keyboard Handler when push a button
  glutKeyboardFunc(KeyboardDown);
  // register Keyboard Handler when release a button
  glutKeyboardUpFunc(KeyboardUp);
  // register Keyboard Handler when push a special button
  glutSpecialFunc(KeyboardSpecialDown);
  // register Keyboard Handler when release a special button
  glutSpecialUpFunc(KeyboardSpecialUp);
  // register Mouse clicks Handler
  glutMouseFunc(MouseClick);
  // register Mouse motion Handler
  glutMotionFunc(MouseMotion);

  // register Menu Handler
  glutCreateMenu(MenuSelect);
  // write menu options
  glutAddMenuEntry("Only Unicorn", UNICORN);
  glutAddMenuEntry("Only Helicopter", HELICOPTER);
  glutAddMenuEntry("Both Objects", BOTH);
  glutAddMenuEntry("Solid Mode", SOLID);
  glutAddMenuEntry("Wireframe Mode", WIRE);
  glutAddMenuEntry("Yellow light", YLIGHT);
  glutAddMenuEntry("Red light", RLIGHT);
  glutAddMenuEntry("Green light", GLIGHT);
  glutAddMenuEntry("Blue light", BLIGHT);
  glutAddMenuEntry("All lights", ALIGHT);
  // attach the menu to the right button
  glutAttachMenu(GLUT_RIGHT_BUTTON);

  // Enter main event handling loop
  glutMainLoop();
  return 0;
}