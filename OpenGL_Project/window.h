/************************************************************************
  Window struct
 ************************************************************************/

struct GlutWindow
{
  int width;
  int height;
  char* title;
  
  float field_of_view_angle;
  float z_near;
  float z_far;
};
