#include <iostream>     // For string handling
#include <iomanip>      // For string manipulation
#include <sstream>      // For string handling
#include <fstream>      // For string handling
#include <cmath>        // For some calculations with special functions
#include "visuals.h"    // Header file for our OpenGL functions
#include "MOdel_OBJ.h"  // Header file for Model_OBJ functions
#include <glut.h>       // An interface and windows management library

enum SYSTEM
{
  CW  = false,
  CCW = true
};

const double PI = 3.141592653589793238462;

using namespace std;

/******************************************************************************* 
  Model_OBJ Methods 
 ******************************************************************************/

Model_OBJ::Model_OBJ(char *filename)
{
  this->filename = filename;
  anomaly = false;
  system = CW;
  net_mode = false;
  TotalTriangles = 0; 
  TotalVertices = 0;
  Center = new Vector3f();
  Max    = new Vector3f();
  Radius = 0;
  rotx = 0;
  roty = 0;
  rotz = 0;
  BoundingSphere = new Sphere*[8];
  AABB           = new Box*[8];
  for (int i = 0; i < 8; i++)
  {
    BoundingSphere[i] = new Sphere();
    AABB[i]           = new Box();
  }
}

Model_OBJ::~Model_OBJ()
{
  cout << endl << " Destructor " << filename <<" here!!" << endl;
  for (long vCounter = 0 ; vCounter < TotalVertices; vCounter++) 
    delete vertexBuffer[vCounter];
  delete[] vertexBuffer;
  for (long tCounter = 0 ; tCounter < TotalTriangles; tCounter++) 
    delete triangleBuffer[tCounter];
  delete[] triangleBuffer;
  delete[] BoundingSphere;
  delete[] AABB;
}

void  Model_OBJ::Load(void)
{
  long tCounter = 0, vCounter = 0;
  
  string line;
  ifstream objFile (filename, ifstream::in);
  if (objFile.is_open())  // If obj file is open, continue
  {
    while (!objFile.eof())  // Start Counting
    {
      getline (objFile,line);  // Get line from file
      if (line.c_str()[0] == 'v') TotalVertices++;  // Count vertices from file
      else if (line.c_str()[0] == 'f') TotalTriangles++;// Count faces from file
    }
    objFile.clear();  // Clear OBJ file flags
    objFile.seekg(0, ios::beg);  // Go back to the beginning of stream
    
    vertexBuffer = new Vector3f*[TotalVertices]; // Allocate memory for the vertices pointers
    triangleBuffer= new Triangle*[TotalTriangles]; // Allocate memory for the triangle pointers
    
    while (!objFile.eof())  // Start reading file data
    {
      getline (objFile,line);  // Get line from file
      
      if (line.c_str()[0] == 'v') // The first character is a v: on this line is a vertex stored.
      {
        vertexBuffer[vCounter] = new Vector3f();// Allocate memory for the vertices
        
        line[0] = ' '; // Set first character to 0. This will allow us to use sscanf_s
        
        sscanf_s(line.c_str(),"%f %f %f ", // Read floats from the line: v X Y Z
          &vertexBuffer[vCounter]->x,
          &vertexBuffer[vCounter]->y,
          &vertexBuffer[vCounter]->z);
          
        vCounter++;                       // Add 1 to the total connected points
      }
      else if (line.c_str()[0] == 'f')    // The first character is an 'f': on this line is a point stored
      {
        triangleBuffer[tCounter] = new Triangle(); // Allocate memory for the triangles
        
        line[0] = ' '; // Set first character to 0. This will allow us to use sscanf_s
        
        sscanf_s(line.c_str(),"%i %i %i",     // Read integers from the line:  f 1 2 3
          &triangleBuffer[tCounter]->vnum1,   // First point of our triangle. This is an 
          &triangleBuffer[tCounter]->vnum2,   // pointer to our vertexBuffer list
          &triangleBuffer[tCounter]->vnum3);  // each point represents an X,Y,Z.
          
        triangleBuffer[tCounter]->vnum1 -= 1; // OBJ file starts counting from 1
        triangleBuffer[tCounter]->vnum2 -= 1; // OBJ file starts counting from 1
        triangleBuffer[tCounter]->vnum3 -= 1; // OBJ file starts counting from 1
        
        triangleBuffer[tCounter]->setMeAsNeighbour(*this);  // Set the neighbours for each vertex
        triangleBuffer[tCounter]->setVertices(*this);       // Calculate all Vertices
        triangleBuffer[tCounter]->setNormal(system);        // Calculate normal CW or CCW
        tCounter++;                    // Add 1 to the total connected triangles
      }
      else if (line.compare("g VIFS01_FACES") == 0)   // The first character is an 'g':
      {anomaly = true; system = CCW;}                 // on this line I read if the object has anomaly, CW or CCW system
    }
    objFile.close();                        // Close OBJ file
    SetAngles();                            // Creates the angle array
  }
  else
  {
    cout << "Unable to open file" << filename << endl;  // Print message
    objFile.close();                                    // Close OBJ file
  }
  
  if ((TotalVertices = vCounter) && (TotalTriangles = tCounter)) // Confirm
    cout << endl
         << " File: " << filename
         << " Loaded Successfully:\n TotalVertices  = "
         << TotalVertices << "\n TotalTriangles = " 
         << TotalTriangles << endl;
}

void  Model_OBJ::DrawModel(void)
{
  // Solid or Wire
  if(net_mode)glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
  else glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
  
  // Cut Triangles
  glEnable(GL_CULL_FACE);
  if (system == CW) glFrontFace(GL_CW);
  else glFrontFace(GL_CCW);
  
  glBegin(GL_TRIANGLES);
    for (int tCounter = 0; tCounter < TotalTriangles; tCounter++)
    {
      if (triangleBuffer[tCounter]->normal)
      {
        glNormal3f( triangleBuffer[tCounter]->normal->x,
              triangleBuffer[tCounter]->normal->y,
              triangleBuffer[tCounter]->normal->z);
      }
      
      if (triangleBuffer[tCounter]->exist)
      {
        if (triangleBuffer[tCounter]->intersected) glColor3f(1.0, 0.0, 0.0);
        else glColor3f(red, green, blue);             // Set triangles Color
        
        glVertex3f( triangleBuffer[tCounter]->vertex1->x,
              triangleBuffer[tCounter]->vertex1->y,
              triangleBuffer[tCounter]->vertex1->z);
        
        glVertex3f( triangleBuffer[tCounter]->vertex2->x,
              triangleBuffer[tCounter]->vertex2->y,
              triangleBuffer[tCounter]->vertex2->z);
        
        glVertex3f( triangleBuffer[tCounter]->vertex3->x,
              triangleBuffer[tCounter]->vertex3->y,
              triangleBuffer[tCounter]->vertex3->z);
      }
    }
  glEnd();
  
  // Reset Mode
  glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
  glDisable(GL_CULL_FACE);
}

void  Model_OBJ::DrawAxes(void)
{
  glColor3f(0.6, 0.6, 0.6);
  glPushMatrix();
    glTranslatef(Center->x, Center->y, Center->z);
    glBegin(GL_LINES);
      glVertex3f(0.0, 0.0, 0.0);
      glVertex3f(100.0, 0.0, 0.0);
      glVertex3f(0.0, 0.0, 0.0);
      glVertex3f(0.0, 100.0, 0.0);
      glVertex3f(0.0, 0.0, 0.0);
      glVertex3f(0.0, 0.0, 100.0);
    glEnd();
    glPushMatrix();
      glTranslatef(90, -2.0 , 0.0);
      glScalef(0.06, 0.06, 0.06);
      glutStrokeCharacter(GLUT_STROKE_ROMAN, 'x');
    glPopMatrix();
    glPushMatrix();
      glTranslatef(-2.0, 90 , 0.0);
      glScalef(0.06, 0.06, 0.06);
      glutStrokeCharacter(GLUT_STROKE_ROMAN, 'y');
    glPopMatrix();
    glPushMatrix();
      glTranslatef(-2.0, -2.0 , 90);
      glScalef(0.06, 0.06, 0.06);
      glutStrokeCharacter(GLUT_STROKE_ROMAN, 'z');
    glPopMatrix();
  glPopMatrix();
}

void  Model_OBJ::DrawSphere(int hierarchy)
{
  Vector3f *center = new Vector3f[8];
  Vector3f sum(0.0f, 0.0f, 0.0f);
  float radius[8];
  long total = 0;
  
  if (hierarchy == 0)
  {
    BoundingSphere[0]->set(this, Center, Radius);
    BoundingSphere[0]->draw();
  }
  else if (hierarchy == 1)
  {
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[0].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[0] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->x < Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[0]) > radius[0])
          radius[0] = vertexBuffer[vCounter]->distance(center[0]);
      }
    }
    BoundingSphere[0]->set(this, &center[0], radius[0]);
    BoundingSphere[0]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[1].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[1] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->x > Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[1]) > radius[1])
          radius[1] = vertexBuffer[vCounter]->distance(center[1]);
      }
    }
    BoundingSphere[1]->set(this, &center[1], radius[1]);
    BoundingSphere[1]->draw();
  }
  else if (hierarchy == 2)
  {
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[0].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[0] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[0]) > radius[0])
          radius[0] = vertexBuffer[vCounter]->distance(center[0]);
      }
    }
    BoundingSphere[0]->set(this, &center[0], radius[0]);
    BoundingSphere[0]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[1].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[1] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[1]) > radius[1])
          radius[1] = vertexBuffer[vCounter]->distance(center[1]);
      }
    }
    BoundingSphere[1]->set(this, &center[1], radius[1]);
    BoundingSphere[1]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[2].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[2] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[2]) > radius[2])
          radius[2] = vertexBuffer[vCounter]->distance(center[2]);
      }
    }
    BoundingSphere[2]->set(this, &center[2], radius[2]);
    BoundingSphere[2]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[3].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[3] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[3]) > radius[3])
          radius[3] = vertexBuffer[vCounter]->distance(center[3]);
      }
    }
    BoundingSphere[3]->set(this, &center[3], radius[3]);
    BoundingSphere[3]->draw();
  }
  else if (hierarchy == 3)
  {
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[0].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[0] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[0]) > radius[0])
          radius[0] = vertexBuffer[vCounter]->distance(center[0]);
      }
    }
    BoundingSphere[0]->set(this, &center[0], radius[0]);
    BoundingSphere[0]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[1].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[1] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[1]) > radius[1])
          radius[1] = vertexBuffer[vCounter]->distance(center[1]);
      }
    }
    BoundingSphere[1]->set(this, &center[1], radius[1]);
    BoundingSphere[1]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[2].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[2] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[2]) > radius[2])
          radius[2] = vertexBuffer[vCounter]->distance(center[2]);
      }
    }
    BoundingSphere[2]->set(this, &center[2], radius[2]);
    BoundingSphere[2]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[3].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[3] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[3]) > radius[3])
          radius[3] = vertexBuffer[vCounter]->distance(center[3]);
      }
    }
    BoundingSphere[3]->set(this, &center[3], radius[3]);
    BoundingSphere[3]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[4].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[4] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[4]) > radius[4])
          radius[4] = vertexBuffer[vCounter]->distance(center[4]);
      }
    }
    BoundingSphere[4]->set(this, &center[4], radius[4]);
    BoundingSphere[4]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[5].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[5] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[5]) > radius[5])
          radius[5] = vertexBuffer[vCounter]->distance(center[5]);
      }
    }
    BoundingSphere[5]->set(this, &center[5], radius[5]);
    BoundingSphere[5]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[6].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[6] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[6]) > radius[6])
          radius[6] = vertexBuffer[vCounter]->distance(center[6]);
      }
    }
    BoundingSphere[6]->set(this, &center[6], radius[6]);
    BoundingSphere[6]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[7].set(sum.x/total, sum.y/total, sum.z/total);
    
    radius[7] = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        if (vertexBuffer[vCounter]->distance(center[7]) > radius[7])
          radius[7] = vertexBuffer[vCounter]->distance(center[7]);
      }
    }
    BoundingSphere[7]->set(this, &center[7], radius[7]);
    BoundingSphere[7]->draw();
  }
  else cout << " That hierarchy does not exist!" << endl;
}

void  Model_OBJ::DrawBox(int hierarchy)
{
  Vector3f *center = new Vector3f[8];
  Vector3f *max    = new Vector3f[8];
  Vector3f sum(0.0f, 0.0f, 0.0f);
  long total = 0;

  CalculateMax();
  if (hierarchy == 0)
  {
    AABB[0]->set(this, Center, Max);
    AABB[0]->draw();
  }
  else if (hierarchy == 1)
  {
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[0].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[0].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->x < Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[0].x) > max[0].x) max[0].x = fabs(vertexBuffer[vCounter]->x - center[0].x);
        if (fabs(vertexBuffer[vCounter]->y - center[0].y) > max[0].y) max[0].y = fabs(vertexBuffer[vCounter]->y - center[0].y);
        if (fabs(vertexBuffer[vCounter]->z - center[0].z) > max[0].z) max[0].z = fabs(vertexBuffer[vCounter]->z - center[0].z);
      }
    }
    AABB[0]->set(this, &center[0], &max[0]);
    AABB[0]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[1].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[1].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->x > Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[1].x) > max[1].x) max[1].x = fabs(vertexBuffer[vCounter]->x - center[1].x);
        if (fabs(vertexBuffer[vCounter]->y - center[1].y) > max[1].y) max[1].y = fabs(vertexBuffer[vCounter]->y - center[1].y);
        if (fabs(vertexBuffer[vCounter]->z - center[1].z) > max[1].z) max[1].z = fabs(vertexBuffer[vCounter]->z - center[1].z);
      }
    }
    AABB[1]->set(this, &center[1], &max[1]);
    AABB[1]->draw();
  }
  else if (hierarchy == 2)
  {
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[0].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[0].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[0].x) > max[0].x) max[0].x = fabs(vertexBuffer[vCounter]->x - center[0].x);
        if (fabs(vertexBuffer[vCounter]->y - center[0].y) > max[0].y) max[0].y = fabs(vertexBuffer[vCounter]->y - center[0].y);
        if (fabs(vertexBuffer[vCounter]->z - center[0].z) > max[0].z) max[0].z = fabs(vertexBuffer[vCounter]->z - center[0].z);
      }
    }
    AABB[0]->set(this, &center[0], &max[0]);
    AABB[0]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[1].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[1].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[1].x) > max[1].x) max[1].x = fabs(vertexBuffer[vCounter]->x - center[1].x);
        if (fabs(vertexBuffer[vCounter]->y - center[1].y) > max[1].y) max[1].y = fabs(vertexBuffer[vCounter]->y - center[1].y);
        if (fabs(vertexBuffer[vCounter]->z - center[1].z) > max[1].z) max[1].z = fabs(vertexBuffer[vCounter]->z - center[1].z);
      }
    }
    AABB[1]->set(this, &center[1], &max[1]);
    AABB[1]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[2].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[2].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[2].x) > max[2].x) max[2].x = fabs(vertexBuffer[vCounter]->x - center[2].x);
        if (fabs(vertexBuffer[vCounter]->y - center[2].y) > max[2].y) max[2].y = fabs(vertexBuffer[vCounter]->y - center[2].y);
        if (fabs(vertexBuffer[vCounter]->z - center[2].z) > max[2].z) max[2].z = fabs(vertexBuffer[vCounter]->z - center[2].z);
      }
    }
    AABB[2]->set(this, &center[2], &max[2]);
    AABB[2]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[3].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[3].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[3].x) > max[3].x) max[3].x = fabs(vertexBuffer[vCounter]->x - center[3].x);
        if (fabs(vertexBuffer[vCounter]->y - center[3].y) > max[3].y) max[3].y = fabs(vertexBuffer[vCounter]->y - center[3].y);
        if (fabs(vertexBuffer[vCounter]->z - center[3].z) > max[3].z) max[3].z = fabs(vertexBuffer[vCounter]->z - center[3].z);
      }
    }
    AABB[3]->set(this, &center[3], &max[3]);
    AABB[3]->draw();
  }
  else if (hierarchy == 3)
  {
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[0].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[0].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[0].x) > max[0].x) max[0].x = fabs(vertexBuffer[vCounter]->x - center[0].x);
        if (fabs(vertexBuffer[vCounter]->y - center[0].y) > max[0].y) max[0].y = fabs(vertexBuffer[vCounter]->y - center[0].y);
        if (fabs(vertexBuffer[vCounter]->z - center[0].z) > max[0].z) max[0].z = fabs(vertexBuffer[vCounter]->z - center[0].z);
      }
    }
    AABB[0]->set(this, &center[0], &max[0]);
    AABB[0]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[1].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[1].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[1].x) > max[1].x) max[1].x = fabs(vertexBuffer[vCounter]->x - center[1].x);
        if (fabs(vertexBuffer[vCounter]->y - center[1].y) > max[1].y) max[1].y = fabs(vertexBuffer[vCounter]->y - center[1].y);
        if (fabs(vertexBuffer[vCounter]->z - center[1].z) > max[1].z) max[1].z = fabs(vertexBuffer[vCounter]->z - center[1].z);
      }
    }
    AABB[1]->set(this, &center[1], &max[1]);
    AABB[1]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[2].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[2].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[2].x) > max[2].x) max[2].x = fabs(vertexBuffer[vCounter]->x - center[2].x);
        if (fabs(vertexBuffer[vCounter]->y - center[2].y) > max[2].y) max[2].y = fabs(vertexBuffer[vCounter]->y - center[2].y);
        if (fabs(vertexBuffer[vCounter]->z - center[2].z) > max[2].z) max[2].z = fabs(vertexBuffer[vCounter]->z - center[2].z);
      }
    }
    AABB[2]->set(this, &center[2], &max[2]);
    AABB[2]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[3].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[3].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z < Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[3].x) > max[3].x) max[3].x = fabs(vertexBuffer[vCounter]->x - center[3].x);
        if (fabs(vertexBuffer[vCounter]->y - center[3].y) > max[3].y) max[3].y = fabs(vertexBuffer[vCounter]->y - center[3].y);
        if (fabs(vertexBuffer[vCounter]->z - center[3].z) > max[3].z) max[3].z = fabs(vertexBuffer[vCounter]->z - center[3].z);
      }
    }
    AABB[3]->set(this, &center[3], &max[3]);
    AABB[3]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[4].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[4].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[4].x) > max[4].x) max[4].x = fabs(vertexBuffer[vCounter]->x - center[4].x);
        if (fabs(vertexBuffer[vCounter]->y - center[4].y) > max[4].y) max[4].y = fabs(vertexBuffer[vCounter]->y - center[4].y);
        if (fabs(vertexBuffer[vCounter]->z - center[4].z) > max[4].z) max[4].z = fabs(vertexBuffer[vCounter]->z - center[4].z);
      }
    }
    AABB[4]->set(this, &center[4], &max[4]);
    AABB[4]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[5].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[5].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y < Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[5].x) > max[5].x) max[5].x = fabs(vertexBuffer[vCounter]->x - center[5].x);
        if (fabs(vertexBuffer[vCounter]->y - center[5].y) > max[5].y) max[5].y = fabs(vertexBuffer[vCounter]->y - center[5].y);
        if (fabs(vertexBuffer[vCounter]->z - center[5].z) > max[5].z) max[5].z = fabs(vertexBuffer[vCounter]->z - center[5].z);
      }
    }
    AABB[5]->set(this, &center[5], &max[5]);
    AABB[5]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[6].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[6].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x < Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[6].x) > max[6].x) max[6].x = fabs(vertexBuffer[vCounter]->x - center[6].x);
        if (fabs(vertexBuffer[vCounter]->y - center[6].y) > max[6].y) max[6].y = fabs(vertexBuffer[vCounter]->y - center[6].y);
        if (fabs(vertexBuffer[vCounter]->z - center[6].z) > max[6].z) max[6].z = fabs(vertexBuffer[vCounter]->z - center[6].z);
      }
    }
    AABB[6]->set(this, &center[6], &max[6]);
    AABB[6]->draw();
    
    sum.set(0.0f, 0.0f, 0.0f);
    total = 0;
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        sum += *vertexBuffer[vCounter];
        total++;
      }
    center[7].set(sum.x/total, sum.y/total, sum.z/total);
    
    max[7].set(0.0f, 0.0f, 0.0f);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      if (vertexBuffer[vCounter]->z > Center->z && vertexBuffer[vCounter]->y > Center->y && vertexBuffer[vCounter]->x > Center->x)
      {
        if (fabs(vertexBuffer[vCounter]->x - center[7].x) > max[7].x) max[7].x = fabs(vertexBuffer[vCounter]->x - center[7].x);
        if (fabs(vertexBuffer[vCounter]->y - center[7].y) > max[7].y) max[7].y = fabs(vertexBuffer[vCounter]->y - center[7].y);
        if (fabs(vertexBuffer[vCounter]->z - center[7].z) > max[7].z) max[7].z = fabs(vertexBuffer[vCounter]->z - center[7].z);
      }
    }
    AABB[7]->set(this, &center[7], &max[7]);
    AABB[7]->draw();
  }
  else cout << " That hierarchy does not exist!" << endl;
}

void  Model_OBJ::SetColor(float red, float green, float blue)
{
  this->red   = red;
  this->green = green;
  this->blue  = blue;
}

void  Model_OBJ::CalibrateTran()
{
  Vector3f Sum(0.0f, 0.0f, 0.0f);
  Vector3f Translation(0.0f, 0.0f, 0.0f);
  
  for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    Sum += *vertexBuffer[vCounter];
  
  Translation.x = Sum.x / TotalVertices;
  Translation.y = Sum.y / TotalVertices;
  Translation.z = Sum.z / TotalVertices;
  
  Translate(-Translation.x, -Translation.y, -Translation.z);
  *Center += Translation;
}

void  Model_OBJ::CalibrateRot(float thx, float thy, float thz)
{
  RotateX(thx);
  RotateY(thy);
  RotateZ(thz);
  rotx -= thx;
  roty -= thy;
  rotz -= thz;
}

void  Model_OBJ::Translate(float dx, float dy, float dz)
{
  Vector3f D(dx, dy, dz);

  for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    *vertexBuffer[vCounter] += D;
  *Center += D;
}

void  Model_OBJ::RotateX(float theta)
{
  float temp[3], th = theta*PI/180.0;
  Vector3f Savecen = *Center;
  float RotMat[3][3]={{1,    0    ,     0    },
                      {0, cosf(th), -sinf(th)},
                      {0, sinf(th),  cosf(th)}};
  if (theta)
  {
    Translate(-Center->x, -Center->y, -Center->z);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      temp[0] = temp[1] = temp[2] = 0;
      temp[0] = RotMat[0][0]*vertexBuffer[vCounter]->x
              + RotMat[0][1]*vertexBuffer[vCounter]->y
              + RotMat[0][2]*vertexBuffer[vCounter]->z;
      temp[1] = RotMat[1][0]*vertexBuffer[vCounter]->x
              + RotMat[1][1]*vertexBuffer[vCounter]->y
              + RotMat[1][2]*vertexBuffer[vCounter]->z;
      temp[2] = RotMat[2][0]*vertexBuffer[vCounter]->x
              + RotMat[2][1]*vertexBuffer[vCounter]->y
              + RotMat[2][2]*vertexBuffer[vCounter]->z;
      
      vertexBuffer[vCounter]->set(temp[0], temp[1], temp[2]);
      
      for (vector<Triangle>::size_type nCounter = 0; nCounter != vertexBuffer[vCounter]->Neighbour.size(); nCounter++)
        vertexBuffer[vCounter]->Neighbour[nCounter]->setNormal(system);
    }
    Translate(Savecen.x, Savecen.y, Savecen.z);
    rotx += theta;
  }
}

void  Model_OBJ::RotateY(float theta)
{
  float temp[3], th = theta*PI/180.0;
  Vector3f Savecen = *Center;
  float RotMat[3][3]={{ cosf(th), 0 , sinf(th)},
                      {    0    , 1 ,    0    },
                      {-sinf(th), 0 , cosf(th)}};
  
  if (theta)
  {
    Translate(-Center->x, -Center->y, -Center->z);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      temp[0] = temp[1] = temp[2] = 0;
      temp[0] = RotMat[0][0]*vertexBuffer[vCounter]->x
              + RotMat[0][1]*vertexBuffer[vCounter]->y
              + RotMat[0][2]*vertexBuffer[vCounter]->z;
      temp[1] = RotMat[1][0]*vertexBuffer[vCounter]->x
              + RotMat[1][1]*vertexBuffer[vCounter]->y
              + RotMat[1][2]*vertexBuffer[vCounter]->z;
      temp[2] = RotMat[2][0]*vertexBuffer[vCounter]->x
              + RotMat[2][1]*vertexBuffer[vCounter]->y
              + RotMat[2][2]*vertexBuffer[vCounter]->z;
      
      vertexBuffer[vCounter]->set(temp[0], temp[1], temp[2]);
      
      for (vector<Triangle>::size_type nCounter = 0; nCounter != vertexBuffer[vCounter]->Neighbour.size(); nCounter++)
        vertexBuffer[vCounter]->Neighbour[nCounter]->setNormal(system);
    }
    Translate(Savecen.x, Savecen.y, Savecen.z);
    roty += theta;
  }
}

void  Model_OBJ::RotateZ(float theta)
{
  float temp[3], th = theta*PI/180.0;
  Vector3f Savecen = *Center;
  float RotMat[3][3]={{cosf(th), -sinf(th), 0},
                      {sinf(th),  cosf(th), 0},
                      {   0       ,  0    , 1}};
  
  if (theta)
  {
    Translate(-Center->x, -Center->y, -Center->z);
    for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
    {
      temp[0] = temp[1] = temp[2] = 0;
      temp[0] = RotMat[0][0]*vertexBuffer[vCounter]->x
              + RotMat[0][1]*vertexBuffer[vCounter]->y
              + RotMat[0][2]*vertexBuffer[vCounter]->z;
      temp[1] = RotMat[1][0]*vertexBuffer[vCounter]->x
              + RotMat[1][1]*vertexBuffer[vCounter]->y
              + RotMat[1][2]*vertexBuffer[vCounter]->z;
      temp[2] = RotMat[2][0]*vertexBuffer[vCounter]->x
              + RotMat[2][1]*vertexBuffer[vCounter]->y
              + RotMat[2][2]*vertexBuffer[vCounter]->z;
      
      vertexBuffer[vCounter]->set(temp[0], temp[1], temp[2]);
      
      for (vector<Triangle>::size_type nCounter = 0; nCounter != vertexBuffer[vCounter]->Neighbour.size(); nCounter++)
        vertexBuffer[vCounter]->Neighbour[nCounter]->setNormal(system);
    }
    Translate(Savecen.x, Savecen.y, Savecen.z);
    rotz += theta;
  }
}

void  Model_OBJ::Scale(float Xscale, float Yscale, float Zscale)
{
  for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
  {
    vertexBuffer[vCounter]->x *= Xscale;
    vertexBuffer[vCounter]->y *= Yscale;
    vertexBuffer[vCounter]->z *= Zscale;
  }
}

void  Model_OBJ::LofD(int factor)
{
  long TrianglesToBeDeleted = TotalTriangles - TotalTriangles/factor;
  long tCounter = 0;
  Vector3f *va, *vb;
  
  cout << endl
       << " Level Of Detail " << filename << " : " << factor << "x" << endl
       << " Estimated Number of Trinagles gonna be deleted: " << TrianglesToBeDeleted << endl
       << " Estimated Number of Remaining Triangles: "<< TotalTriangles/factor << endl
       << " Please wait..." << endl;
  ResetModel();
  while (tCounter < TrianglesToBeDeleted)
  {
    va = DefineDeleteVertex();
    vb = DefineReplaceVertex(va);
    if (vb) tCounter += Replace(va, vb);
  }
  cout << " Number of Triangles finally Deleted : " << tCounter << endl;
  cout << " Done!!!" << endl;
}

void  Model_OBJ::SetAngles(void)
{
  vector<Triangle>::size_type nCounter;
  
  for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
  {
    for (nCounter = 0; nCounter != (vertexBuffer[vCounter]->Neighbour.size()-1); nCounter++)
    {
      vertexBuffer[vCounter]->angle += CalculateAngle(vertexBuffer[vCounter]->Neighbour[nCounter]->normal,
                        vertexBuffer[vCounter]->Neighbour[nCounter+1]->normal);
    }
    vertexBuffer[vCounter]->angle += CalculateAngle(vertexBuffer[vCounter]->Neighbour[nCounter]->normal,
                        vertexBuffer[vCounter]->Neighbour[0]->normal);
    if (vertexBuffer[vCounter]->Neighbour.size() != 0) vertexBuffer[vCounter]->angle /= vertexBuffer[vCounter]->Neighbour.size();
    else vertexBuffer[vCounter]->angle = 0.0;
  }
}

void  Model_OBJ::ResetModel(void)
{
  for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
  {
    vertexBuffer[vCounter]->exist = true;
    vertexBuffer[vCounter]->Neighbour.clear();
  }
  
  for (long tCounter = 0; tCounter < TotalTriangles; tCounter++)
  {
    triangleBuffer[tCounter]->exist  = true;
    triangleBuffer[tCounter]->change = false;
    triangleBuffer[tCounter]->setMeAsNeighbour(*this);          // Set the neighbours for each vertex
    triangleBuffer[tCounter]->setVertices(*this);           // Calculate old Vertices
    triangleBuffer[tCounter]->setNormal(system);            // Calculate old normals
  }
}

Vector3f* Model_OBJ::DefineDeleteVertex(void)
{
  long minposition;
  float minangle = 10.0;
  
  for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
  {
    if (vertexBuffer[vCounter]->exist)
    {
      if (vertexBuffer[vCounter]->angle < minangle)
      {
        minangle = vertexBuffer[vCounter]->angle;
        minposition = vCounter;
      }
    }
  }
  return vertexBuffer[minposition];
}

Vector3f* Model_OBJ::DefineReplaceVertex(Vector3f *va)
{
  for (vector<Triangle>::size_type nCounter = 0; nCounter != va->Neighbour.size(); nCounter++)
  {
    if (va->Neighbour[nCounter]->exist)
    {
      if (va == va->Neighbour[nCounter]->vertex1 && !va->Neighbour[nCounter]->change)
      {
        if (va->distance(*va->Neighbour[nCounter]->vertex2) < va->distance(*va->Neighbour[nCounter]->vertex3))
          return va->Neighbour[nCounter]->vertex2;
        else return va->Neighbour[nCounter]->vertex3;
      }
      else if (va == va->Neighbour[nCounter]->vertex2)
      {
        if (va->distance(*va->Neighbour[nCounter]->vertex1) < va->distance(*va->Neighbour[nCounter]->vertex3))
          return va->Neighbour[nCounter]->vertex1;
        else return va->Neighbour[nCounter]->vertex3;
      }
      else if (va == va->Neighbour[nCounter]->vertex3)
      {
        if (va->distance(*va->Neighbour[nCounter]->vertex1) < va->distance(*va->Neighbour[nCounter]->vertex2))
          return va->Neighbour[nCounter]->vertex1;
        else return va->Neighbour[nCounter]->vertex2;
      }
    }
  }
  for (vector<Triangle>::size_type nCounter = 0; nCounter != va->Neighbour.size(); nCounter++)
  {
    if (va->Neighbour[nCounter]->exist)
    {
      if (va == va->Neighbour[nCounter]->vertex1)
      {
        if (va->distance(*va->Neighbour[nCounter]->vertex2) < va->distance(*va->Neighbour[nCounter]->vertex3))
          return va->Neighbour[nCounter]->vertex2;
        else return va->Neighbour[nCounter]->vertex3;
      }
      else if (va == va->Neighbour[nCounter]->vertex2)
      {
        if (va->distance(*va->Neighbour[nCounter]->vertex1) < va->distance(*va->Neighbour[nCounter]->vertex3))
          return va->Neighbour[nCounter]->vertex1;
        else return va->Neighbour[nCounter]->vertex3;
      }
      else if (va == va->Neighbour[nCounter]->vertex3)
      {
        if (va->distance(*va->Neighbour[nCounter]->vertex1) < va->distance(*va->Neighbour[nCounter]->vertex2))
          return va->Neighbour[nCounter]->vertex1;
        else return va->Neighbour[nCounter]->vertex2;
      }
    }
  }
  va->exist = false;
  return nullptr;
}

long  Model_OBJ::Replace(Vector3f *va, Vector3f *vb)
{
  long trianglesDeleted = 0;
  bool deleteVertex = true;
  
  for (vector<Triangle>::size_type nCounter = 0; nCounter != va->Neighbour.size(); nCounter++)
  {
    if (va->Neighbour[nCounter]->exist)
    {
      if ((  va == va->Neighbour[nCounter]->vertex1
        || va == va->Neighbour[nCounter]->vertex2
        || va == va->Neighbour[nCounter]->vertex3)
        &&(vb == va->Neighbour[nCounter]->vertex1
        || vb == va->Neighbour[nCounter]->vertex2
        || vb == va->Neighbour[nCounter]->vertex3))
      {
        va->Neighbour[nCounter]->exist = false;
        trianglesDeleted++;
      }
      else
      {
        va->Neighbour[nCounter]->change = true;
        if (va == va->Neighbour[nCounter]->vertex1)
          va->Neighbour[nCounter]->vertex1 = vb;
        else if (va == va->Neighbour[nCounter]->vertex2)
          va->Neighbour[nCounter]->vertex2 = vb;
        else if (va == va->Neighbour[nCounter]->vertex3)
          va->Neighbour[nCounter]->vertex3 = vb;
        va->Neighbour[nCounter]->setNormal(system);         // Calculate new normal
      }
    }
  }
  vb->Neighbour.insert(vb->Neighbour.end(),               // Add va Neighbours to vb Neighbours
    va->Neighbour.begin(), va->Neighbour.end());
  
  for (vector<Triangle>::size_type nCounter = 0; nCounter != va->Neighbour.size(); nCounter++)
  {
    if (va->Neighbour[nCounter]->exist)
    {
      if (   va == va->Neighbour[nCounter]->vertex1
        || va == va->Neighbour[nCounter]->vertex2
        || va == va->Neighbour[nCounter]->vertex3)
      deleteVertex = false;
    }
  }
  if (deleteVertex) va->exist = false;
  return trianglesDeleted;
}

float Model_OBJ::CalculateAngle(Vector3f *va, Vector3f *vb)
{
  float dotval, vala, valb, value, angle = 0.0;
  
  if (va && vb)
  {
    dotval = va->dot(*vb);            // dot product
    vala = va->length();              // va normalization factor
    valb = vb->length();              // vb normalization factor
    
    if (vala != 0 && valb != 0)       // not actually needed coz
    {                                 // va and vb are normals so |va|=|vb|=1
      value = dotval/(vala*valb);
      if (fabs(value) > 1.0) value = 0.0;               // Handle acosf exeption case
      angle = acosf(value);
    }
  }
  return angle;
}

void  Model_OBJ::CalculateMax(void)
{
  float maxx = 0, maxy = 0, maxz = 0;
  for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
  {
    if (fabs(vertexBuffer[vCounter]->x - Center->x) > maxx) maxx = fabs(vertexBuffer[vCounter]->x - Center->x);
    if (fabs(vertexBuffer[vCounter]->y - Center->y) > maxy) maxy = fabs(vertexBuffer[vCounter]->y - Center->y);
    if (fabs(vertexBuffer[vCounter]->z - Center->z) > maxz) maxz = fabs(vertexBuffer[vCounter]->z - Center->z);
  }
  Max->set(maxx, maxy, maxz);
}

void  Model_OBJ::CalculateRadius(void)
{
  for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
  {
    if (vertexBuffer[vCounter]->distance(*Center) > Radius)
      Radius = vertexBuffer[vCounter]->distance(*Center);
  }
}

void  Model_OBJ::DetectCollisions(const Model_OBJ &other)
{
  long cCounter = 0;
  
  for (long tCounter = 0; tCounter < TotalTriangles; tCounter++)
    triangleBuffer[tCounter]->intersected = false;
  for (long tOther = 0; tOther < other.TotalTriangles; tOther++)
    other.triangleBuffer[tOther]->intersected = false;
  
  cout << endl
     << " Number of collisions is calculating..." << endl;
  for (long tCounter = 0; tCounter < TotalTriangles; tCounter++)
    if (triangleBuffer[tCounter]->exist && triangleBuffer[tCounter]->normal)
      for (long tOther = 0; tOther < other.TotalTriangles; tOther++)
        if (other.triangleBuffer[tOther]->exist && other.triangleBuffer[tOther]->normal)
          if (triangleBuffer[tCounter]->intersect(*other.triangleBuffer[tOther], system))
            cCounter++;
  cout << " Total collisions between models : " << cCounter << endl;
}

void  Model_OBJ::SphereCoverage()
{
  Vector3f vCenter(Center->x, Center->y, Center->z);
  Vector3f vMax(Center->x + Radius, Center->y + Radius, Center->z + Radius);
  Vector3f vPoint;
  float step  = 2.0;
  long  total = 0, active = 0, numOfIntersects;
  
  cout << endl
       << " Bounding sphere percentage volume coverage " << endl
       << " of " << filename << " is calculating..." << endl;
  for (float x = (Center->x - Radius); x < (Center->x + Radius); x += step)
    for (float y = (Center->y - Radius); y < (Center->y + Radius); y += step)
      for (float z = (Center->z - Radius); z < (Center->z + Radius); z += step)
      {
        vPoint.set(x, y, z);
        if (vPoint.distance(vCenter) < Radius)
        {
          total++;
          numOfIntersects = 0;
          for (long tCounter = 0; tCounter < TotalTriangles; tCounter++)
            if (triangleBuffer[tCounter]->intersect(vPoint, vMax, system))
              numOfIntersects++;
          if ((!anomaly && numOfIntersects%2) || (anomaly && numOfIntersects%2))
            active++;
        }
      }
  if (total)
  cout << " Coverage Percentage : " << 100.0*active/total << "% ("
     << active << "/" << total << ")" << endl;
  else
  cout << " Draw bounding sphere first (press x)" << endl;
}

void  Model_OBJ::BoxCoverage()
{
  Vector3f vMax(Center->x+Max->x, Center->y+Max->y, Center->z+Max->z);  // Set the top right point
  Vector3f vPoint;                              // My current sample point
  float step  = 2.0;                            // Distance between sample points
  long  total = 0, active = 0, numOfIntersects; // Counts total and active points
  
  cout << endl
       << " Axis aligned bounding box percentage volume coverage " << endl
       << " of " << filename << " is calculating..." << endl;
  for (float x = (Center->x - Max->x); x < (Center->x + Max->x); x += step)// Scan the whole cube
    for (float y = (Center->y - Max->y); y < (Center->y + Max->y); y += step)
      for (float z = (Center->z - Max->z); z < (Center->z + Max->z); z += step)
      {
        vPoint.set(x, y, z);                    // Set the current point
        total++;
        numOfIntersects = 0;
        for (long tCounter = 0; tCounter < TotalTriangles; tCounter++)
          if (triangleBuffer[tCounter]->intersect(vPoint, vMax, system))
            numOfIntersects++;                  // Count the number of intersects between model and line(vPoint, vCenter)
        if ((!anomaly && numOfIntersects%2) || (anomaly && numOfIntersects%2))
          active++;
      }
  if (total)                                // If maxx, maxy, maxz have been set
  cout << " Coverage Percentage : " << 100.0*active/total << "% ("
     << active << "/" << total << ")" << endl;
  else
  cout << " Draw the axis aligned bounding box first (press z)" << endl;
}

void  Model_OBJ::PrintTriangles(void)
{
  long total = 0;
  cout << endl << " triangleBuffer of: "  << filename  << endl
       << "\t\tVertex1\t\t\tVertex2\t\t\t\tVertex3\t\t\t\tNormal" << endl;
  for (long tCounter = 0; tCounter < TotalTriangles; tCounter++)
  {
    if (triangleBuffer[tCounter]->exist)
    {
      if (triangleBuffer[tCounter]->vertex1 && triangleBuffer[tCounter]->vertex1->exist)
      {
        cout << setiosflags(ios::fixed) << setprecision(5)
             << setw(9) << triangleBuffer[tCounter]->vertex1->x << "  "
             << setw(9) << triangleBuffer[tCounter]->vertex1->y << "  "
             << setw(9) << triangleBuffer[tCounter]->vertex1->z << "\t";
      }
      else if (!triangleBuffer[tCounter]->vertex1) cout << setw(29) << "NULL\t";
      else if (!triangleBuffer[tCounter]->vertex1->exist) cout << setw(19) << tCounter << ":  NOT EXIST\t";
      
      if (triangleBuffer[tCounter]->vertex2 && triangleBuffer[tCounter]->vertex2->exist)
      {
        cout << setiosflags(ios::fixed) << setprecision(5)
             << setw(9) << triangleBuffer[tCounter]->vertex2->x << "  "
             << setw(9) << triangleBuffer[tCounter]->vertex2->y << "  "
             << setw(9) << triangleBuffer[tCounter]->vertex2->z << "\t";
      }
      else if (!triangleBuffer[tCounter]->vertex2) cout << setw(29) << "NULL\t";
      else if (!triangleBuffer[tCounter]->vertex2->exist) cout << setw(19) << tCounter << ":  NOT EXIST\t";
      
      if (triangleBuffer[tCounter]->vertex3 && triangleBuffer[tCounter]->vertex3->exist)
      {
        cout << setiosflags(ios::fixed) << setprecision(5)
           << setw(9) << triangleBuffer[tCounter]->vertex3->x << "  "
           << setw(9) << triangleBuffer[tCounter]->vertex3->y << "  "
           << setw(9) << triangleBuffer[tCounter]->vertex3->z << "\t";
      }
      else if (!triangleBuffer[tCounter]->vertex3) cout << setw(29) << "NULL\t";
      else if (!triangleBuffer[tCounter]->vertex3->exist) cout << setw(19) << tCounter << ":  NOT EXIST\t";
      
      if (triangleBuffer[tCounter]->normal)
      {
        cout << setiosflags(ios::fixed) << setprecision(5)
             << setw(9) << triangleBuffer[tCounter]->normal->x << "  "
             << setw(9) << triangleBuffer[tCounter]->normal->y << "  "
             << setw(9) << triangleBuffer[tCounter]->normal->z << endl;
      }
      else cout << setw(20) << "NULL" << endl;
      total++;
    }
  }
  cout << " Total Triangles : " << total << endl;
}

void  Model_OBJ::PrintVertices(void)
{
  long total = 0;
  cout << endl << " vertexBuffer of: " << filename << endl
       << "    X\t\t    Y\t\t    Z\t    NeighBours\t  Angle" << endl;
  for (long vCounter = 0; vCounter < TotalVertices; vCounter++)
  {
    if (vertexBuffer[vCounter]->exist)
    {
      cout << setiosflags(ios::fixed) << setprecision(5)
           << setw(9) << vertexBuffer[vCounter]->x << "\t"
           << setw(9) << vertexBuffer[vCounter]->y << "\t"
           << setw(9) << vertexBuffer[vCounter]->z << "\t"
           << vertexBuffer[vCounter]->Neighbour.size()<< "\t"
           << setw(9) << vertexBuffer[vCounter]->angle<< endl;
      total++;
    }
    else cout << "VERTEX: " << vCounter << " DOES NOT EXIST" << endl;
  }
  cout << " Total Vertices : " << total << endl;
}


/******************************************************************************* 
  Triangle Methods 
 ******************************************************************************/

Triangle::Triangle()
{
  exist   = true;
  change  = false;
  intersected=false;
  vertex1 = nullptr;
  vertex2 = nullptr;
  vertex3 = nullptr;
  normal  = nullptr;
}

Triangle::~Triangle()
{
  exist = false;
  delete normal;
}

void Triangle::setMeAsNeighbour(Model_OBJ &model)
{
  model.vertexBuffer[vnum1]->Neighbour.push_back(this);
  model.vertexBuffer[vnum2]->Neighbour.push_back(this);
  model.vertexBuffer[vnum3]->Neighbour.push_back(this);
}

void Triangle::setVertices(Model_OBJ &model)
{
  vertex1 = model.vertexBuffer[vnum1];
  vertex2 = model.vertexBuffer[vnum2];
  vertex3 = model.vertexBuffer[vnum3];
}

void Triangle::setNormal(bool system)
{
  Vector3f va, vb, crossProduct;
  
  if(system == CW)                            // calculate va and vb depends on system CW or CCW
  {va = (*vertex3 - *vertex1); vb = (*vertex2 - *vertex1);}
  else
  {va = (*vertex2 - *vertex1); vb = (*vertex3 - *vertex1);}
  
  crossProduct = va.cross(vb);
  if (crossProduct.length() != 0)
  {
    if (!normal) normal  = new Vector3f;                // Allocate memory for this normal
    
    crossProduct.normalize();
    *normal = crossProduct;
  }
  else normal = nullptr;
}

bool Triangle::intersect(Triangle &other, bool system)
{
  if (normal && other.normal)
  {
    if (intersect(*other.vertex1, *other.vertex2, system))
    {intersected = true; other.intersected = true; return true;}
    if (intersect(*other.vertex2, *other.vertex3, system))
    {intersected = true; other.intersected = true; return true;}
    if (intersect(*other.vertex3, *other.vertex1, system))
    {intersected = true; other.intersected = true; return true;}
  }
  return false;
}

bool Triangle::intersect(Vector3f &lp1, Vector3f &lp2, bool system)
{
  if (normal)
  {
    Vector3f IntersectPos, vTest;
    
    float Dist1 = normal->dot(lp1 - *vertex1);              // Find distance from lp1 and lp2 to the plane defined by the triangle
    float Dist2 = normal->dot(lp2 - *vertex1);
    
    if ((Dist1 * Dist2) >= 0.0f) return false;              // line doesn't cross the triangle.
    if ((Dist1 == Dist2)) return false;                 // line and plane are parallel
    
    IntersectPos = lp1 + (lp2 - lp1) * (-Dist1/(-Dist1 + Dist2));   // Find point on the line that intersects with the plane
    
    if (system == CW)                         // Find if the interesection point lies inside the triangle by testing it against all edges
    {
      vTest = normal->cross(*vertex2 - *vertex1);
      if (vTest.dot(IntersectPos - *vertex1) > 0.0f) return false;
      
      vTest = normal->cross(*vertex3 - *vertex2);
      if (vTest.dot(IntersectPos - *vertex2) > 0.0f) return false;
      
      vTest = normal->cross(*vertex1 - *vertex3);
      if (vTest.dot(IntersectPos - *vertex3) > 0.0f) return false;
    }
    else
    {
      vTest = normal->cross(*vertex2 - *vertex1);
      if (vTest.dot(IntersectPos - *vertex1) < 0.0f) return false;
      
      vTest = normal->cross(*vertex3 - *vertex2);
      if (vTest.dot(IntersectPos - *vertex2) < 0.0f) return false;
      
      vTest = normal->cross(*vertex1 - *vertex3);
      if (vTest.dot(IntersectPos - *vertex3) < 0.0f) return false;
    }
    
    return true;                            // Finally Ray intersects triangle at IntersectPos
  }
  return false;
}

/******************************************************************************* 
  Vector3f Methods 
 ******************************************************************************/

Vector3f::Vector3f()
{
  exist = true;
  angle = 0.0f;
  x = 0.0f;
  y = 0.0f;
  z = 0.0f;
}

Vector3f::Vector3f(float x, float y, float z)
{
  exist = true;
  angle = 0.0f;
  this->x = x;
  this->y = y;
  this->z = z;
}

Vector3f::~Vector3f()
{
  exist = false;
}

void  Vector3f::set(float x, float y, float z)
{
  this->x = x;
  this->y = y;
  this->z = z;
}

float Vector3f::length(void)
{
  return( (float)sqrt( x * x + y * y + z * z ) );
}

void  Vector3f::normalize(void)
{
  float fLength = length();
  
  x = x / fLength;
  y = y / fLength;
  z = z / fLength;
}

Vector3f Vector3f::operator + (const Vector3f &other)
{
  Vector3f vResult(0.0f, 0.0f, 0.0f);
  
  vResult.x = x + other.x;
  vResult.y = y + other.y;
  vResult.z = z + other.z;
  
  return vResult;
}

Vector3f Vector3f::operator + (void) const
{
  return *this;
}

Vector3f Vector3f::operator - (const Vector3f &other)
{
  Vector3f vResult(0.0f, 0.0f, 0.0f);
  
  vResult.x = x - other.x;
  vResult.y = y - other.y;
  vResult.z = z - other.z;
  
  return vResult;
}

Vector3f Vector3f::operator - (void) const
{
  Vector3f vResult(-x, -y, -z);
  
  return vResult;
}

Vector3f Vector3f::operator * (const Vector3f &other)
{
  Vector3f vResult(0.0f, 0.0f, 0.0f);
  
  vResult.x = x * other.x;
  vResult.y = y * other.y;
  vResult.z = z * other.z;
  
  return vResult;
}

Vector3f Vector3f::operator * (const float scalar)
{
  Vector3f vResult(0.0f, 0.0f, 0.0f);
  
  vResult.x = x * scalar;
  vResult.y = y * scalar;
  vResult.z = z * scalar;
  
  return vResult;
}

Vector3f operator * (const float scalar, const Vector3f &other)
{
  Vector3f vResult(0.0f, 0.0f, 0.0f);
  
  vResult.x = other.x * scalar;
  vResult.y = other.y * scalar;
  vResult.z = other.z * scalar;
  
  return vResult;
}

Vector3f Vector3f::operator / (const Vector3f &other)
{
  Vector3f vResult(0.0f, 0.0f, 0.0f);
  
  vResult.x = x / other.x;
  vResult.y = y / other.y;
  vResult.z = z / other.z;
  
  return vResult;
}

Vector3f& Vector3f::operator = (const Vector3f &other)
{
  x = other.x;
  y = other.y;
  z = other.z;
  
  return *this;
}

Vector3f& Vector3f::operator += (const Vector3f &other)
{
  x += other.x;
  y += other.y;
  z += other.z;
  
  return *this;
}

Vector3f& Vector3f::operator -= (const Vector3f &other)
{
  x -= other.x;
  y -= other.y;
  z -= other.z;
  
  return *this;
}

float Vector3f::distance(const Vector3f &other)
{
  float dx = x - other.x;
  float dy = y - other.y;
  float dz = z - other.z;
  
  return (float)sqrt( dx * dx + dy * dy + dz * dz );
}

float Vector3f::dot(const Vector3f &other)
{
  return(x * other.x + y * other.y + z * other.z);
}

Vector3f Vector3f::cross(const Vector3f &other)
{
  Vector3f vCrossProduct;
  
  vCrossProduct.x =  y * other.z - z * other.y;
  vCrossProduct.y = -x * other.z + z * other.x;
  vCrossProduct.z =  x * other.y - y * other.x;
  
  return vCrossProduct;
}

/******************************************************************************* 
  Sphere Methods 
 ******************************************************************************/

Sphere::Sphere()
{
  center = new Vector3f();
  radius = 0.0;
  model  = nullptr;
}

Sphere::~Sphere()
{
  delete center;
}

void  Sphere::set(Model_OBJ *model, Vector3f *center, float radius)
{
  this->model  = model;
  this->center = center;
  this->radius = radius;
}

void  Sphere::draw()
{
  glColor4f(model->red, model->green, model->blue, 0.5);
  glPushMatrix();
    glTranslatef(center->x, center->y, center->z);
    glutSolidSphere(radius, 36, 36);
  glPopMatrix();
}

void  Sphere::coverage()
{
  Vector3f vMax(center->x + radius, center->y + radius, center->z + radius);// Set the top right point
  Vector3f vPoint;                            // My current sample point
  float step  = 2.0;                            // Distance between sample points
  long  total = 0, active = 0, numOfIntersects;
  
  if (model)
  {
    cout << endl
         << " Bounding sphere percentage volume coverage " << endl
         << " of " << model->filename << " is calculating..." << endl;
    for (float x = (center->x - radius); x < (center->x + radius); x += step)// Scan the whole shere
      for (float y = (center->y - radius); y < (center->y + radius); y += step)
        for (float z = (center->z - radius); z < (center->z + radius); z += step)
        {
          vPoint.set(x, y, z);                  // Set the current point
          if (vPoint.distance(*center) < radius)
          {
            total++;
            numOfIntersects = 0;                // Count the number of intersects between model and line(vPoint, vCenter)
            for (long tCounter = 0; tCounter < model->TotalTriangles; tCounter++)
              if (model->triangleBuffer[tCounter]->intersect(vPoint, vMax, model->system))
                numOfIntersects++;
            if (numOfIntersects%2)
              active++;
          }
        }
    if (total)                              // If maxx, maxy, maxz have been set
    cout << " Coverage Percentage : " << 100.0*active/total << "% ("
         << active << "/" << total << ")" << endl;
    else
    cout << " Draw bounding sphere first (press x)" << endl;
  }
  else cout << " Draw bounding sphere first (press x)" << endl;
}

/******************************************************************************* 
  Box Methods 
 ******************************************************************************/

Box::Box()
{
  center = new Vector3f();
  max    = new Vector3f();
  model  = nullptr;
}

Box::~Box()
{
  delete center;
  delete max;
}

void  Box::set(Model_OBJ *model, Vector3f *center, Vector3f *max)
{
  this->model  = model;
  this->center = center;
  this->max    = max;
}

void  Box::draw()
{
  glColor4f(model->red, model->green, model->blue, 0.5);
  glPushMatrix();
    glTranslatef(center->x, center->y, center->z);
    glScalef(2*max->x, 2*max->y, 2*max->z);
    glutSolidCube(1.0);
  glPopMatrix();
}

void  Box::coverage()
{
  Vector3f vMax;                              // Set the top right point
  Vector3f vPoint;                            // My current sample point
  float step  = 2.0;                            // Distance between sample points
  long  total = 0, active = 0, numOfIntersects;             // Counts total and active points
  
  if (model)
  {
    cout << endl
         << " Axis aligned bounding box percentage volume coverage " << endl
         << " of " << model->filename << " is calculating..." << endl;
    vMax = *model->Center + *model->Max;
    for (float x = (center->x - max->x); x < (center->x + max->x); x += step)// Scan the whole cube
      for (float y = (center->y - max->y); y < (center->y + max->y); y += step)
        for (float z = (center->z - max->z); z < (center->z + max->z); z += step)
        {
          vPoint.set(x, y, z);                  // Set the current point
          total++;
          numOfIntersects = 0;
          for (long tCounter = 0; tCounter < model->TotalTriangles; tCounter++)
            if (model->triangleBuffer[tCounter]->intersect(vPoint, vMax, model->system))
              numOfIntersects++;                // Count the number of intersects between model and line(vPoint, vCenter)
          if (numOfIntersects%2)
            active++;
        }
    if (total)                              // If maxx, maxy, maxz have been set
    cout << " Coverage Percentage : " << 100.0*active/total << "% ("
         << active << "/" << total << ")" << endl;
    else
    cout << " Draw the axis aligned bounding box first (press z)" << endl;
  }
  else cout << " Draw the axis aligned bounding box first (press z)" << endl;
}